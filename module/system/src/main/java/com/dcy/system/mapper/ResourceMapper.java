package com.dcy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dcy.system.model.Resource;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 资源表 Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2020-08-19
 */
public interface ResourceMapper extends BaseMapper<Resource> {

    /**
     * 根据角色id 查询权限标识
     *
     * @param roleId 角色id
     * @return
     */
    List<String> selectPermissionListByRoleId(@Param("roleId") String roleId);

    /**
     * 根据用户id 查询权限列表
     *
     * @param userId
     * @return
     */
    List<Resource> selectAuthResourcesListByUserId(@Param("userId") String userId);
}
